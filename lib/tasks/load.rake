require 'csv'
namespace :load do
  desc 'Carga de información en CSV'
  task csv: [:environment] do

    #######################################################
    # AL
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/al.csv"
    dim_source = DimSource.where(source: 'Asamblea Legislativa').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
      DimEndorsement.destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[5].try(:strip), charge: row[6].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[1]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[12]) rescue nil) || time,
          arrival_date: (Date.parse(row[13]) rescue nil) || time,
          objectives: row[7]
        )
        if fact.save
          # fact travel is valid, we save the relationship models
          DimEndorsement.create(
            fact_travel_id: fact.id,
            dl_date: (Date.parse(row[1]) rescue nil),
            do_date: (Date.parse(row[4]) rescue nil),
            tome: row[3],
            dl: row[0],
            do: row[2]
          )
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[8], city: row[9]) if row[8].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[10], city: row[11]) if row[10].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end

    #######################################################
    # EMP
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/emp.csv"
    dim_source = DimSource.where(source: 'Estado Mayor Presidencial').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[0].try(:strip), charge: row[1].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[6]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[6]) rescue nil),
          arrival_date: (Date.parse(row[7]) rescue nil)
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[2], city: row[3]) if row[2].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[4], city: row[5]) if row[4].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end


    #######################################################
    # MIGRACION
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/migracion.csv"
    dim_source = DimSource.where(source: 'Dirección General de Migración y Extranjería').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[2].try(:strip), charge: row[1].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[0]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create
        is_departure = (row[3].to_s == 'ENTRADA' ? false : true)

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[0]) rescue nil),
          arrival_date: (Date.parse(row[0]) rescue nil),
          is_departure: is_departure ? 1 : 0,
          objectives: row[10]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[7]) if row[7].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end


    #######################################################
    # RELACIONES EXTERIORES
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/rel.csv"
    dim_source = DimSource.where(source: 'Ministerio de Relaciones Exteriores').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[1].try(:strip), charge: row[0].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[2]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[2]) rescue nil),
          arrival_date: (Date.parse(row[3]) rescue nil),
          objectives: row[5]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[4]) if row[4].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end


    #######################################################
    # JURIDICA
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/salj.csv"
    dim_source = DimSource.where(source: 'Secretaría Jurídica').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[1].try(:strip), charge: row[0].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[3]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[3]) rescue nil),
          arrival_date: (Date.parse(row[4]) rescue nil),
          objectives: row[7],
          designate: row[6],
          agreement: row[2]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[9], city: row[8]) if row[9].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[11], city: row[10]) if row[11].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[13], city: row[12]) if row[13].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end

    #######################################################
    # UFI
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/ufi.csv"
    dim_source = DimSource.where(source: 'UFI').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[1].upcase.try(:strip), charge: row[0].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[3]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[6]) rescue nil) || time,
          arrival_date: (row[11].present? ? (Date.parse(row[11]) rescue nil) : ( Date.parse(row[7]) rescue nil)) || time,
          viaticum: row[2],
          viaticum_date: (Date.parse(row[3]) rescue nil)
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[5], city: row[4]) if row[5].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[9], city: row[8]) if row[9].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[13], city: row[12]) if row[13].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end
  end

  desc 'Corrige los IDs de los actores'
  task fix_actors: [:environment] do
    # Calderon Sol
    FactTravel.where(dim_actor_id: [24, 25]).update_all(dim_actor_id: 3)
    DimActor.where(id: [24, 25]).destroy_all
    # Antonio Saca
    FactTravel.where(dim_actor_id: [14, 26]).update_all(dim_actor_id: 9)
    DimActor.where(id: [14, 26]).destroy_all
    # Sanchez Ceren
    FactTravel.where(dim_actor_id: [23]).update_all(dim_actor_id: 6)
    DimActor.where(id: [23]).destroy_all
    # Mauricio Funes
    FactTravel.where(dim_actor_id: [27]).update_all(dim_actor_id: 7)
    DimActor.where(id: [27]).destroy_all
    # Ana Ligia de Saca
    FactTravel.where(dim_actor_id: [10]).update_all(dim_actor_id: 11)
    DimActor.where(id: [10]).destroy_all
    # Elizabeth Calderón
    FactTravel.where(dim_actor_id: [28]).update_all(dim_actor_id: 15)
    DimActor.where(id: [28]).destroy_all
    # Lourdes de Flores
    FactTravel.where(dim_actor_id: [29]).update_all(dim_actor_id: 17)
    DimActor.where(id: [29]).destroy_all
    # Ana Vilma
    FactTravel.where(dim_actor_id: [12]).update_all(dim_actor_id: 31)
    DimActor.where(id: [12]).destroy_all
    # Carlos Schmidt
    FactTravel.where(dim_actor_id: [13]).update_all(dim_actor_id: 30)
    DimActor.where(id: [13]).destroy_all
    # José Merino
    FactTravel.where(dim_actor_id: [2]).update_all(dim_actor_id: 16)
    DimActor.where(id: [2]).destroy_all
    # Sanchez Ceren
    FactTravel.where(dim_actor_id: [32]).update_all(dim_actor_id: 22)
    DimActor.where(id: [32]).destroy_all
    # Hide a actor charge
    DimActor.where(charge: 'OTROS').update_all(active: false)
  end

  desc 'Agrega los periodos a los actores'
  task add_period: [:environment] do
    DimActor.where(id: [1,16,18]).update_all(period: '1989-1994') # Cristiani
    DimActor.where(id: [3,4,15]).update_all(period: '1994-1999') # Sol
    DimActor.where(id: [5,17,30]).update_all(period: '1999-2004') # Paco
    DimActor.where(id: [9,11,31]).update_all(period: '2004-2009') # Saca
    DimActor.where(id: [7,8,21,22]).update_all(period: '2009-2014') # Funes
    DimActor.where(id: [6,19,20]).update_all(period: '2014-2019') # Profe
  end

  desc 'Carga de información EDH'
  task edh: [:environment] do
    i = 0
    csv_path = "#{Rails.root.to_s}/db/edh.csv"
    dim_source = DimSource.where(source: 'El Diario de Hoy').first_or_create
    # Clean models
    if dim_source
      FactTravel.where(dim_source_id: dim_source.id).destroy_all
    end
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[0].try(:strip), charge: row[1].try(:strip)).first
        # Second, we get the time dimension
        time = Date.parse(row[2]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[2]) rescue nil),
          arrival_date: (Date.parse(row[3]) rescue nil),
          objectives: row[9],
          companions_count: row[5],
          companions: row[6]
        )
        if fact.save
          # Destinations
          if row[7].present?
            d = fact.dim_destinations.create(country: row[7], city: row[8])
          end
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end
  end

  desc 'Carga nueva información jurídica'
  task salj: [:environment] do
    #######################################################
    # JURIDICA 2
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/salj_2.csv"
    salj_source = DimSource.where(source: 'Secretaría Jurídica').first_or_create
    daci_source = DimSource.where(source: 'DACI').first_or_create
    ufi_source = DimSource.where(source: 'UFI').first_or_create
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[1].try(:strip), charge: row[0].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[5]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the salj register
        fact = FactTravel.new(
          dim_source_id: salj_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[5]) rescue nil) || time,
          arrival_date: (Date.parse(row[6]) rescue nil) || time,
          objectives: row[7],
          agreement: row[2]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[8]) if row[8].present?
        end

        # Create the DACI register
        fact = FactTravel.new(
          dim_source_id: daci_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[5]) rescue nil) || time,
          arrival_date: (Date.parse(row[6]) rescue nil) || time,
          objectives: row[7],
          cost: row[3]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[8]) if row[8].present?
        end

        # Create the UFI register
        fact = FactTravel.new(
          dim_source_id: ufi_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[5]) rescue nil) || time,
          arrival_date: (Date.parse(row[6]) rescue nil) || time,
          objectives: row[7],
          viaticum: row[4]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[8]) if row[8].present?
        end

      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end
  end

  desc 'Carga información DACI'
  task daci: [:environment] do
    #######################################################
    # DACI
    #######################################################
    i = 0
    csv_path = "#{Rails.root.to_s}/db/daci.csv"
    dim_source = DimSource.where(source: 'DACI').first_or_create
    CSV.foreach(csv_path, headers: true, encoding:'utf-8') do |row|
      i = i + 1
      begin
        # First, we create an Actor
        dim_actor = DimActor.where(name: row[1].try(:strip), charge: row[0].try(:strip)).first_or_create
        # Second, we get the time dimension
        time = Date.parse(row[4]) rescue nil
        dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create

        # Create the fact travels register
        fact = FactTravel.new(
          dim_source_id: dim_source.try(:id),
          dim_actor_id: dim_actor.try(:id),
          dim_time_id: dim_time.try(:id),
          departure_date: (Date.parse(row[4]) rescue nil) || time,
          arrival_date: (Date.parse(row[5]) rescue nil) || time,
          objectives: row[3],
          cost: row[2]
        )
        if fact.save
          # Destinations
          DimDestination.create(fact_travel_id: fact.id, country: row[6], city: row[7]) if row[6].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[8], city: row[9]) if row[8].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[10], city: row[11]) if row[10].present?
          DimDestination.create(fact_travel_id: fact.id, country: row[12], city: row[13]) if row[12].present?
        end
      rescue Exception => e
        puts "Error en la línea #{i}. #{e}"
      end
    end
    # Manually create a register
    dim_actor = DimActor.where(name: 'ROSA MARGARITA VILLALTAA DE SÁNCHEZ', charge: 'PRIMERA DAMA').first_or_create
    time = Date.parse('26/05/2015') rescue nil
    dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create
    fact = FactTravel.new(
      dim_source_id: dim_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('26/05/2015') rescue nil) || time,
      arrival_date: (Date.parse('27/05/2015') rescue nil) || time,
      cost: 1278.75
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Nicaragua', city: 'Managua')
    end
  end

  desc 'Reload DATA'
  task restart: [:environment] do
    Rake::Task["db:migrate VERSION=0"].invoke
    Rake::Task["db:migrate"].invoke
    Rake::Task["load:csv"].invoke
    Rake::Task["load:fix_actors"].invoke
    Rake::Task["load:add_period"].invoke
    Rake::Task["load:edh"].invoke
    Rake::Task["load:salj"].invoke
    Rake::Task["load:daci"].invoke
    Rake::Task["load:oir"].invoke
    DimActor.where(charge: 'OTROS').update_all(:active, false)
  end

  desc 'OIR modifications'
  task oir: [:environment] do
    # Manually create registers
    salj_source = DimSource.where(source: 'Secretaría Jurídica').first_or_create
    daci_source = DimSource.where(source: 'DACI').first_or_create
    ufi_source = DimSource.where(source: 'UFI').first_or_create
    dim_actor = DimActor.where(name: 'SALVADOR SÁNCHEZ CERÉN', charge: 'PRESIDENTE DE LA REPÚBLICA').first_or_create
    # First Travel DACI
    time = Date.parse('29/11/2016') rescue nil
    dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create
    fact = FactTravel.new(
      dim_source_id: daci_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('29/11/2016') rescue nil) || time,
      arrival_date: (Date.parse('30/11/2016') rescue nil) || time,
      cost: 2278.03,
      objectives: 'Asistir al de Honras Fúnebres del Ex Presidente Fidel Castro'
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Cuba', city: 'La Habana')
    end
    # First Travel SALJ
    fact = FactTravel.new(
      dim_source_id: salj_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('29/11/2016') rescue nil) || time,
      arrival_date: (Date.parse('30/11/2016') rescue nil) || time,
      agreement: 'NO. 685, DE FECHA 28 DE NOVIEMBRE DE 2016',
      objectives: 'Asistir al de Honras Fúnebres del Ex Presidente Fidel Castro'
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Cuba', city: 'La Habana')
    end
    # Second Travel DACI
    dim_actor = DimActor.where(name: 'SALVADOR SÁNCHEZ CERÉN', charge: 'PRESIDENTE DE LA REPÚBLICA').first_or_create
    time = Date.parse('19/12/2016') rescue nil
    dim_time = DimTime.where(year: time.try(:year), month: time.try(:month)).first_or_create
    fact = FactTravel.new(
      dim_source_id: daci_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('19/12/2016') rescue nil) || time,
      arrival_date: (Date.parse('21/12/2016') rescue nil) || time,
      cost: 1179.42,
      objectives: 'Asistir al XLVIII Cumbre de Jefes de Estado y de Gobierno del Sistema de Integración Centroamericana SICA'
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Nicaragua', city: 'Managua')
    end
    # Second Travel UFI
    dim_source = DimSource.where(source: 'UFI').first_or_create
    fact = FactTravel.new(
      dim_source_id: ufi_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('19/12/2016') rescue nil) || time,
      arrival_date: (Date.parse('21/12/2016') rescue nil) || time,
      viaticum: 920,
      viaticum_date: (Date.parse('19/12/2016') rescue nil) || time,
      objectives: 'Asistir al XLVIII Cumbre de Jefes de Estado y de Gobierno del Sistema de Integración Centroamericana SICA'
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Nicaragua', city: 'Managua')
    end
    # Second Travel SALJ
    dim_source = DimSource.where(source: 'UFI').first_or_create
    fact = FactTravel.new(
      dim_source_id: salj_source.try(:id),
      dim_actor_id: dim_actor.try(:id),
      dim_time_id: dim_time.try(:id),
      departure_date: (Date.parse('19/12/2016') rescue nil) || time,
      arrival_date: (Date.parse('21/12/2016') rescue nil) || time,
      agreement: 'NO. 717, DE FECHA 15 DE DICIEMBRE DE 2016',
      objectives: 'Asistir al XLVIII Cumbre de Jefes de Estado y de Gobierno del Sistema de Integración Centroamericana SICA'
    )
    if fact.save
      # Destinations
      DimDestination.create(fact_travel_id: fact.id, country: 'Nicaragua', city: 'Managua')
    end

    # Delete a travel
    FactTravel.find(3300).destroy
    FactTravel.find(3301).destroy
    FactTravel.find(3302).destroy

    # Modify travels
    DimDestination.where(fact_travel_id: 3365).first.update_column(:country, 'CUBA')
    DimDestination.where(fact_travel_id: 3363).first.update_column(:country, 'CUBA')
    DimDestination.where(fact_travel_id: 3364).first.update_column(:country, 'CUBA')
    FactTravel.where(id: 3374).first.update_column(:viaticum, 1545)
    FactTravel.where(id: 3377).first.update_column(:viaticum, 1585)
    FactTravel.where(id: 3380).first.update_column(:viaticum, 1545)
    FactTravel.where(id: 3371).first.update_column(:viaticum, 0)


  end

end
