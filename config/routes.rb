Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'travels#index'

  resources :travels, only: [:index]
  get ':dim_actor_id/:dim_source_id', to: 'travels#show', as: :travel
end
