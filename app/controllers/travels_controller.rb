#
class TravelsController < ApplicationController
  #
  before_action :init_table
  #
  def index
  end

  def show
    @fact_travels = FactTravel
                    .competent
                    .where(
                      params.permit(
                        :dim_actor_id,
                        :dim_source_id
                      )
                    )
                    .order(:departure_date)
    render template: 'travels/index'
  end

  def init_table
    @dim_actors  = DimActor.active.charge_order.period_order
    @dim_sources = DimSource.order(:source)
  end
end
