#
class DimActor < ApplicationRecord
  ##
  # Validations
  validates :name, presence: true
  ##
  # Associations
  has_many :fact_travels

  scope :charge_order, lambda {
    order(
      'CASE charge
        WHEN \'PRESIDENTE DE LA REPÚBLICA\' THEN 1
        WHEN \'VICEPRESIDENTE\' THEN 2
        WHEN \'PRIMERA DAMA\' THEN 3
        WHEN \'OTROS\' THEN 4
        ELSE 5
     END'
    )
  }

  scope :period_order, lambda {
    order('LEFT(period, STRPOS(period, \'-\') - 1)::int')
  }

  scope :active, -> { where(active: true) }
end
