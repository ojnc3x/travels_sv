#
class DimDestination < ApplicationRecord
  #
  belongs_to :fact_travel

  def name
    cols = [city, country]
    return 'N/A' if cols.all?(&:blank?)

    sep = ', '
    sep = '' if cols.any?(&:blank?)
    [city, country].join(sep)
  end
end
