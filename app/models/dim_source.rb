#
class DimSource < ApplicationRecord
  ##
  # Associations
  has_many :fact_travels

  NAMES = {
    1 => 'Asamblea',
    2 => 'EMP',
    3 => 'Migración',
    4 => 'RREE',
    5 => 'Jurídica',
    6 => 'CAPRES',
    7 => 'Medios',
    8 => 'DACI'
  }

  def acronym
    #source.scan(/\p{Upper}/).join('')
    NAMES[id]
  end
end
