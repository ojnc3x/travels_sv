#
class FactTravel < ApplicationRecord
  ##
  # Validations
  validates :dim_time_id, :dim_actor_id, :dim_source_id, presence: true

  ##
  # Associations
  has_many :dim_destinations, dependent: :destroy
  belongs_to :dim_source

  scope :competent, lambda {
    where.not(departure_date: nil)
         .where.not(arrival_date: nil)
  }

  scope :actor_id, -> (actor_id) { where(dim_actor_id: actor_id) }

  scope :distincts, -> { select('distinct on (departure_date) *') }

  scope :overlapping, lambda { |other|
    where(
      [
        '(departure_date - ?) * (? - arrival_date) >= 0',
        other.arrival_date,
        other.departure_date
      ]
    )
  }

  def details
    str = ''
    str += "\nObjetivos: #{objectives}" if objectives.present?
    if dim_destinations.any?
      str += "\nDestinos: #{dim_source_id == 3 ? (is_departure != 1 ? 'Entrada a ' : 'Salida a ' ) : ''}#{dim_destinations.collect{|o| [o.country, o.city].compact.join(', ')}.join('. ')}"
    end
    if companions.present?
      str += "\nAcompañantes: #{companions_count.to_i > 0 ? "#{companions_count} en total. " : ''}#{companions}"
    end
    if cost.present?
      str += "\nCosto del boleto: $#{cost}."
    end
    if viaticum.present?
      str += "\nViáticos: $#{viaticum}."
      str += " Pagados el: #{viaticum_date.try(:strftime, '%d-%m-%Y')}." if viaticum_date
    end
    # str += .... if condition

    # If nothing was concated
    str = 'N/A' if str.blank?
    str
  end

  MIGRATION = {
    1 => 4,
    3 => 50,
    4 => 60,
    5 => 45,
    6 => 34,
    7 => 70,
    8 => 103,
    9 => 79,
    11 => 64,
    15 => 42,
    16 => 1,
    17 => 54,
    18 => 4,
    19 => 11,
    20 => 11,
    22 => 53,
    30 => 65,
    31 => 90
  }
end
