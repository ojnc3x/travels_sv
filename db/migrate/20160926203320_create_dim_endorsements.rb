class CreateDimEndorsements < ActiveRecord::Migration[5.0]
  def change
    create_table :dim_endorsements do |t|
      t.references :fact_travel, index: true
      t.integer :dl
      t.date :dl_date
      t.integer :do
      t.date :do_date
      t.integer :tome
    end
  end
end
