class CreateDimTimes < ActiveRecord::Migration[5.0]
  def change
    create_table :dim_times do |t|
      t.integer :period, index: true
      t.integer :year
      t.integer :month
      t.string :month_str
      t.string :period_str
    end
  end
end
