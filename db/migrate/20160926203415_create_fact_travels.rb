class CreateFactTravels < ActiveRecord::Migration[5.0]
  def change
    create_table :fact_travels do |t|
      t.references :dim_source, index: true
      t.references :dim_actor, index: true
      t.references :dim_time, index: true
      t.integer :is_departure, default: 1
      t.date :departure_date
      t.date :arrival_date
      t.decimal :amount, precision: 12, scale: 2
      t.string :objectives
      t.string :agreement
      t.string :designate
    end
    add_foreign_key :fact_travels, :dim_sources
    add_foreign_key :fact_travels, :dim_actors
    add_foreign_key :fact_travels, :dim_times
    add_foreign_key :dim_destinations, :fact_travels
  end
end
