class CreateDimActors < ActiveRecord::Migration[5.0]
  def change
    create_table :dim_actors do |t|
      t.string :charge
      t.string :name, index: true
    end
  end
end
