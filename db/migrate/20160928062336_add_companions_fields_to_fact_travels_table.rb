class AddCompanionsFieldsToFactTravelsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :fact_travels, :companions_count, :integer, default: 0
    add_column :fact_travels, :companions, :string
  end
end
