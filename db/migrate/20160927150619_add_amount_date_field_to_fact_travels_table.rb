class AddAmountDateFieldToFactTravelsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :fact_travels, :amount_date, :date
  end
end
