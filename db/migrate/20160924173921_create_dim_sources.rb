class CreateDimSources < ActiveRecord::Migration[5.0]
  def change
    create_table :dim_sources do |t|
      t.string :source
    end
  end
end
