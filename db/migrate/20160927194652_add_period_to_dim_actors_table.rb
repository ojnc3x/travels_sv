class AddPeriodToDimActorsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :dim_actors, :period, :string
    add_index :dim_actors, :period
  end
end
