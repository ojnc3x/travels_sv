class CreateDimDestinations < ActiveRecord::Migration[5.0]
  def change
    create_table :dim_destinations do |t|
      t.references :fact_travel, index: true
      t.string :country, index: true
      t.string :city
    end
  end
end
