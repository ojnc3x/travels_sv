class ChangeAmountFieldsInTravelsTable < ActiveRecord::Migration[5.0]
  def change
    rename_column :fact_travels, :amount, :viaticum
    rename_column :fact_travels, :amount_date, :viaticum_date
    add_column :fact_travels, :cost, :decimal, precision: 12, scale: 2
  end
end
