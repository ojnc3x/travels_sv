class AddActiveToDimActor < ActiveRecord::Migration[5.0]
  def change
    add_column :dim_actors, :active, :boolean, null: false, default: true
  end
end
